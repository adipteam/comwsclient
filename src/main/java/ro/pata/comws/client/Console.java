package ro.pata.comws.client;

import org.springframework.beans.factory.annotation.Autowired;

public class Console {
    @Autowired
    ComWs service;
    @Autowired
    ComManager comMan;

    public void process(String command){
        String[] com=command.split(" ");

        switch (com[0]){
            case "listservice":
                showServicePorts();
                break;
            case "listlocal":
                showLocalPorts();
                break;
            case "con":
                connectTo(com[1],com[2]);
                break;
            case "send":
                sendTestData(Integer.parseInt(com[1]));
                break;
            case "disc":
                disconnectAll();
                break;
            default:
                System.out.println("Unknown command.");
        }
    }

    private void disconnectAll() {
        service.disconnectAll();
    }

    private void sendTestData(int port) {
        service.tx(port,"Cotoi Vasile".getBytes());
    }

    private void showLocalPorts() {
        for(String p:comMan.getPorts()){
            System.out.println(p);
        }
    }

    private void connectTo(String portService,String portLocal) {
        int pService=service.connectTo(portService);
        if(pService>=0){
            System.out.println("Connected to service port "+portService+" on ["+pService+"]");
            if(comMan.connectTo(pService,portLocal)) {
                System.out.println("Connected to local port "+portLocal);
            } else {
                System.out.println("Could not connect to local port "+portLocal);
            }
        } else {
            System.out.println("Could not connect to service port "+portService);
        }
    }

    private void showServicePorts(){
        for(String p:service.getPortNames()){
            System.out.println(p);
        }
    }
}
