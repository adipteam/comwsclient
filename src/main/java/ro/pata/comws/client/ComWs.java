package ro.pata.comws.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ro.pata.comws.server.ws.ComPortWebservice;

import java.util.List;

public class ComWs {

    ComPortWebservice port;

    public List<String> getPortNames(){
        return port.getServicePortNames();
    }

    public int connectTo(String portName){
        return port.connectTo(portName);
    }

    public void tx(int portNo,byte[] data){
        port.sendData(portNo,data);
    }

    public void disconnectAll(){port.disconnectAll();}

    public byte[] rx(int portNo){ return port.getReceivedData(portNo);}

    @SuppressWarnings({"SpringJavaAutowiringInspection", "SpringJavaAutowiredMembersInspection", "SpringJavaInjectionPointsAutowiringInspection"})
    @Autowired
    @Qualifier("service")
    public void setPort(ComPortWebservice port){
        this.port=port;
    }

}
