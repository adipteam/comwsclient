package ro.pata.comws.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Client application that opens a local COM port and links it with a remote one using a webservice.
 */
public class App {
    public static ApplicationContext context;

    public static void main(String[] args) {
        context= new ClassPathXmlApplicationContext("Beans.xml");
        Console console=(Console)context.getBean("console");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String line;
            do {
                System.out.print(">");
                line = reader.readLine();
                console.process(line);
            }while(!line.equals("exit"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
