package ro.pata.comws.client;

import com.fazecast.jSerialComm.SerialPort;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

public class ComManager {
    @Autowired
    ComWs service;
    List<ComLocal> connectedPorts=new ArrayList<>();
    Thread sendDataThread;
    Thread receiveDataThread;

    @PostConstruct
    public void init(){
        sendDataThread=new Thread(new sendData());
        sendDataThread.start();

        receiveDataThread=new Thread(new receiveData());
        receiveDataThread.start();
    }

    public List<String> getPorts(){
        List<String> portNames=new ArrayList<>();
        SerialPort[] ports=SerialPort.getCommPorts();

        for(SerialPort p:ports) portNames.add(p.getSystemPortName());

        return portNames;
    }

    public boolean connectTo(int servicePort, String localPortName){
        SerialPort[] ports=SerialPort.getCommPorts();

        for(SerialPort p:ports) {
            if (p.getSystemPortName().equals(localPortName)) {
                ComLocal coml=new ComLocal(p,servicePort);
                if(!connectedPorts.contains(coml)){
                    connectedPorts.add(coml);
                    coml.start();
                    return true;
                }
            }
        }
        return false;
    }

    private class receiveData implements  Runnable{

        public boolean alive=true;
        int waittime=1000;

        @Override
        public void run() {
            while (alive){
                for(ComLocal port:connectedPorts){
                    byte[] data=service.rx(port.getServicePort());
                    if(data.length>0){
                        port.tx(data);
                        waittime=100;
                    } else {
                        if(waittime<3000) waittime+=100;
                    }
                }

                try {
                    Thread.sleep(waittime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class sendData implements Runnable{

        public boolean alive=true;

        @Override
        public void run() {
            while(alive){
                for(ComLocal p:connectedPorts){
                    if(p.hasReceivedData()){
                        service.tx(p.getServicePort(),p.getReceivedData());
                    }
                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
