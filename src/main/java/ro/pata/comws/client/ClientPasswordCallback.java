package ro.pata.comws.client;

import java.util.Map;
import java.util.HashMap;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;
import org.apache.wss4j.common.ext.WSPasswordCallback;

/**
 * Client-side password callback
 *
 * User: magnusmickelsson
 * Date: 2010-aug-27
 * Time: 14:19:21
 */
public class ClientPasswordCallback implements CallbackHandler
{

    private Map<String, String> passwords = new HashMap<String, String>();

    public ClientPasswordCallback()
    {
        passwords.put("client", "salam");
    }

    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException
    {
        for (Callback callback : callbacks)
        {
            WSPasswordCallback passwordCallback = (WSPasswordCallback) callback;

            String password = passwords.get(passwordCallback.getIdentifier());
            if (password != null)
            {
                passwordCallback.setPassword(password);
                return;
            }
        }
    }
}
