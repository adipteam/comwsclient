package ro.pata.comws.client;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

import java.util.*;

public class ComLocal {
    private SerialPort port;
    private int servicePort;
    private Deque<byte[]> receivedData=new ArrayDeque<>();

    public ComLocal(SerialPort port, int servicePort) {
        this.port = port;
        this.servicePort=servicePort;
    }

    public void start(){
        port.setBaudRate(9600);
        port.openPort();
        port.addDataListener(new serialPortDataListener());
    }

    public boolean hasReceivedData(){
        return receivedData.size()>0;
    }

    public void tx(byte[] data){
        port.writeBytes(data,data.length);
    }

    public byte[] getReceivedData(){
        List<Byte> recb=new ArrayList<>();
        while(receivedData.size()>0){
            byte[] data=receivedData.removeLast();
            for(byte b:data) recb.add(b);
        }

        byte[] data=new byte[recb.size()];
        for(int i=0;i<recb.size();i++) data[i]=recb.get(i);

        return data;
    }

    public int getServicePort(){
        return servicePort;
    }

    private class serialPortDataListener implements SerialPortDataListener{

        @Override
        public int getListeningEvents() {
            return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
        }

        @Override
        public void serialEvent(SerialPortEvent event) {
            if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                return;
            byte[] newData = new byte[port.bytesAvailable()];
            port.readBytes(newData, newData.length);
            receivedData.addFirst(newData);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComLocal comLocal = (ComLocal) o;
        return port.getSystemPortName().equals(comLocal.port.getSystemPortName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(port.getSystemPortName());
    }
}
